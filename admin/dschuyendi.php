<?php
 
// Kết nối database và thông tin chung
require_once 'core/init.php';
 
// Nếu đăng nhập
if ($user) 
{
    // Nếu tồn tại POST action
    if (isset($_POST['action']))
    {
        // Xử lý POST action
        $action = trim(addslashes(htmlspecialchars($_POST['action'])));
 
        // Thêm chuyến đi
        if ($action == 'add_chuyendi')
        {
            // Xử lý các giá trị
            $diemdon_add_dschuyendi = trim(addslashes(htmlspecialchars($_POST['diemdon_add_dschuyendi'])));
            $diemden_add_dschuyendi = trim(addslashes(htmlspecialchars($_POST['diemden_add_dschuyendi'])));
            $thoigiandon_add_dschuyendi = trim(addslashes(htmlspecialchars($_POST['thoigiandon_add_dschuyendi'])));
            $gia_add_dschuyendi = trim(addslashes(htmlspecialchars($_POST['gia_add_dschuyendi'])));
 
            // Các biến xử lý thông báo
            $show_alert = '<script>$("#formAddChuyenDi .alert").removeClass("hidden");</script>';
            $hide_alert = '<script>$("#formAddChuyenDi .alert").addClass("hidden");</script>';
            $success = '<script>$("#formAddChuyenDi .alert").attr("class", "alert alert-success");</script>';
 
            // Nếu các giá trị rỗng
            if ($diemdon_add_dschuyendi == '' || $diemden_add_dschuyendi == ''|| $thoigiandon_add_dschuyendi == '' || $gia_add_dschuyendi == '')
            {
                echo $show_alert.'Vui lòng điền đầy đủ thông tin';
            }
            // Ngược lại
            else
            {
                // Kiểm tra chuyến đi tồn tại 
                $sql_check_chuyendi_exist = "SELECT diadiemdon, diadiemden, thoigiandon FROM danhsachchuyendi WHERE diemdiemdon = '$diemdon_add_dschuyendi' OR diemdiemden = '$diemden_add_dschuyendi' OR thoigiandon = '$thoigiandon_add_dschuyendi'";
                // Nếu chuyến đi tồn tại
                if ($db->num_rows($sql_check_chuyendi_exist))
                {
                    echo $show_alert.'Chuyến đi đã tồn tại.';
                }
                else
                {
                    // Thực thi thêm chuyến đi
                    // Thực thi thêm chuyến đi
                    $sql_add_chuyendi = "INSERT INTO danhsachchuyendi VALUES (
                        '',
                        '$diemdon_add_dschuyendi',
                        '$diemden_add_dschuyendi',
                        '$thoigiandon_add_dschuyendi',
                        '0',
                        '$date_current',
                        '$gia_add_dschuyendi'

                    )";
                    $db->query($sql_add_chuyendi);
                    echo $show_alert.$success.'Thêm chuyến đi thành công.';
                    $db->close(); // Giải phóng
                    new Redirect($_DOMAIN.'danhsachchuyendi'); // Trở về trang danh sách chuyến đi
                }
            }
        }
 
        
        
        // Chỉnh sửa chuyến đi
        else if ($action == 'edit_chuyendi')
        {
            // Xử lý các giá trị
            $id_chuyendi = trim(htmlspecialchars(addslashes($_POST['id_chuyendi'])));
            $stt_edit_chuyendi = trim(htmlspecialchars(addslashes($_POST['stt_edit_chuyendi'])));
            $diadiemdon_edit_chuyendi = trim(htmlspecialchars(addslashes($_POST['diadiemdon_edit_chuyendi'])));
            $diadiemden_edit_chuyendi = trim(htmlspecialchars(addslashes($_POST['diadiemden_edit_chuyendi'])));
            $thoigiandon_edit_chuyendi = trim(htmlspecialchars(addslashes($_POST['thoigiandon_edit_chuyendi'])));
            $gia_edit_chuyendi = trim(htmlspecialchars(addslashes($_POST['gia_edit_chuyendi'])));
            // Các biến xử lý thông báo
            $show_alert = '<script>$("#formEditChuyenDi .alert").removeClass("hidden");</script>';
            $hide_alert = '<script>$("#formEditChuyenDi .alert").addClass("hidden");</script>';
            $success = '<script>$("#formEditChuyenDi .alert").attr("class", "alert alert-success");</script>';
        
            // Kiểm tra id chuyến đi
            $sql_check_id_chuyendi = "SELECT id_chuyendi FROM danhsachchuyendi WHERE id_chuyendi = '$id_chuyendi'";
        
            // Nếu các giá trị rỗng
            if ($stt_edit_chuyendi == '' || $diadiemdon_edit_chuyendi == '' || $diadiemden_edit_chuyendi == '' || $thoigiandon_edit_chuyendi == '' || $gia_edit_chuyendi == '') 
            {
                echo $show_alert.'Vui lòng điền đầy đủ thông tin.';
            } 
            else if (!$db->num_rows($sql_check_id_chuyendi))
            {
                echo $show_alert.'Đã có lỗi xảy ra, vui lòng thử lại.';
            }
            else
            {
                // Sửa chuyến đi
                $sql_edit_chuyendi = " UPDATE danhsachchuyendi SET 
                id_chuyendi = '$id_chuyendi', 
                diadiemdon  = '$diadiemdon_edit_chuyendi' ,
                diadiemden  =  '$diadiemden_edit_chuyendi' ,
                thoigiandon = '$thoigiandon_edit_chuyendi' ,
                status      = '$stt_edit_chuyendi' ,
                date_posted =  now(),
                gia         = '$gia_edit_chuyendi'
                WHERE id_chuyendi = '$id_chuyendi'
                ";
                echo $id_chuyendi.$stt_edit_chuyendi. $diadiemdon_edit_chuyendi. $diadiemden_edit_chuyendi. $thoigiandon_edit_chuyendi. $gia_edit_chuyendi;
                $db->query($sql_edit_chuyendi);
                $db->close();
                echo $show_alert.$success.'Chỉnh sửa chuyến đi thành công.';
                new Redirect($_DOMAIN.'danhsachchuyendi&page='.$id_chuyendi);
            }
        }
        
        // Xoá chuyến đi
        // Xoá nhiều chuyến đi cùng lúc
        else if ($action == 'delete_post_list')
        {
            foreach ($_POST['id_chuyendi'] as $key => $id_chuyendi)
            {
                $sql_check_id_chuyendi_exist = "SELECT id_chuyendi FROM danhsachchuyendi WHERE id_chuyendi = '$id_chuyendi'";
                if ($db->num_rows($sql_check_id_chuyendi_exist))
                {
                    $sql_delete_chuyendi = "DELETE FROM danhsachchuyendi WHERE id_chuyendi = '$id_chuyendi'";
                    $db->query($sql_delete_chuyendi);
                }
            }   
            $db->close();
        }
        // Xoá 1 chuyên mục
        else if ($action == 'delete_post')
        {       
            $id_chuyendi = trim(htmlspecialchars(addslashes($_POST['id_chuyendi'])));
            $sql_check_id_chuyendi_exist = "SELECT id_chuyendi FROM danhsachchuyendi WHERE id_chuyendi = '$id_chuyendi'";
            if ($db->num_rows($sql_check_id_chuyendi_exist))
            {
                $sql_delete_chuyendi = "DELETE FROM danhsachchuyendi WHERE id_chuyendi = '$id_chuyendi'";
                $db->query($sql_delete_chuyendi);
                $db->close();
            }       
        }
        // Tìm kiếm chuyến đi
        else if ($action == 'search_chuyendi')
        {
            $kw_search_chuyendi = trim(htmlspecialchars(addslashes($_POST['kw_search_chuyendi'])));
        
            if ($kw_search_chuyendi != '')
            {
                $sql_search_chuyendi = "SELECT * FROM danhsachchuyendi WHERE 
                    id_chuyendi LIKE '%$kw_search_chuyendi%' OR
                    diadiemdon LIKE '%$kw_search_chuyendi%' OR
                    diadiemden LIKE '%$kw_search_chuyendi%'
                    ORDER BY id_chuyendi DESC
                ";
        
                // Nếu có kết quả
                if ($db->num_rows($sql_search_chuyendi)) 
                {
                    echo
                    '
                        <table class="table table-striped list">
                            <tr>
                                <td><input type="checkbox"></td>
                                <td><strong>ID</strong></td>
                                <td><strong>Địa Điểm Đón</strong></td>
                                <td><strong>Địa Điểm Đến</strong></td>
                                <td><strong>Trạng Thái</strong></td>
                                <td><strong>Thời Gian Đón</strong></td>
                                <td><strong>Giá</strong></td>
                    ';
        
                    
        
                    echo '
                                    <td><strong>Tools</strong></td>
                                </tr>
                    ';
        
                    // In danh sách kết quả chuyến đi
                    foreach ($db->fetch_assoc($sql_search_chuyendi, 0) as $key => $data_chuyendi) 
                    {
                        // Trạng thái chuyến đi
                        if ($data_chuyendi['status'] == 0) {
                            $stt_chuyendi = '<label class="label label-warning">Ẩn</label>';
                        } else if ($data_chuyendi['status'] == 1) {
                            $stt_chuyendi = '<label class="label label-success">Active</label>';
                        }
        
                        
        
                        echo
                        '
                            <tr>
                                <td><input type="checkbox" name="id_chuyendi[]" value="' . $data_chuyendi['id_chuyendi'] .'"></td>
                                <td>' . $data_chuyendi['id_chuyendi'] . '</td>
                                <td>' . $data_chuyendi['diadiemdon'] . '</td>
                                <td>' . $data_chuyendi['diadiemden'] . '</td>
                                <td>' . $stt_chuyendi . '</td>
                                <td>' . $data_chuyendi['thoigiandon'] . '</td>
                                <td>' . $data_chuyendi['gia'] . '</td>
                        ';
        
                        
        
                        echo '
                                <td>
                                    <a href="' . $_DOMAIN . 'danhsachchuyendi/edit/' . $data_chuyendi['id_chuyendi'] .'" class="btn btn-primary btn-sm">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                    <a class="btn btn-danger btn-sm del-post-list" data-id="' . $data_chuyendi['id_chuyendi'] . '">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        ';
                    }
                    echo '</table>';
                } 
                // Ngược lại không có kết quả
                else
                {
                    echo '<div class="alert alert-info">Không tìm thấy kết quả nào cho từ khoá <strong>' . $kw_search_chuyendi . '</strong>.</div>';
                }
            }
        }
    }
    // Ngược lại không tồn tại POST action
    else
    {
        new Redirect($_DOMAIN);
    }
}
// Nếu không đăng nhập
else
{
    new Redirect($_DOMAIN);
}
 
?>
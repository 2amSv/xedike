<?php include('server.php') ?>
<!DOCTYPE html>
<html>
<head>
  <title>Xe Đi Ké</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="header" style="width:22%;">
  	<h2>Đăng Kí Tài Khoản</h2>
  </div>
	
  <form method="post" action="register.php" style="width:22%;">
  	<?php include('errors.php'); ?>
  	<div class="input-group">
  	  <label>Tên đăng nhập</label>
  	  <input type="text" name="username" value="<?php echo $username; ?>">
  	</div>
  	<div class="input-group">
  	  <label>Email</label>
  	  <input type="email" name="email" value="<?php echo $email; ?>">
  	</div>
  	<div class="input-group">
  	  <label>Password</label>
  	  <input type="password" name="password_1">
  	</div>
  	<div class="input-group">
  	  <label>Confirm password</label>
  	  <input type="password" name="password_2">
  	</div>
  	<div class="input-group">
  	  <button type="submit" class="btn" name="reg_user">Đăng kí</button>
  	</div>
  	<p>
  		Bạn là thành viên chưa? <a href="login.php">Sign in</a>
  	</p>
  </form>
</body>
</html>
<?php
  
// Nếu đăng nhập
if ($user)
{
    echo '<h3>Thêm Chuyến Đi</h3>';
    // Lấy tham số ac
    if (isset($_GET['ac']))
    {
        $ac = trim(addslashes(htmlspecialchars($_GET['ac'])));
    }
    else
    {
        $ac = '';
    }
  
    // Lấy tham số id
    if (isset($_GET['id']))
    {
        $id = trim(addslashes(htmlspecialchars($_GET['id'])));
    }
    else
    {
        $id = '';
    }
  
    // Nếu có tham số ac
    if ($ac != '') 
    {
        // Trang thêm chuyến đi
        if ($ac == 'add')
        {
            // Dãy nút của thêm chuyến đi
            echo
            '
                <a href="' . $_DOMAIN . 'danhsachchuyendi" class="btn btn-default">
                    <span class="glyphicon glyphicon-arrow-left"></span> Trở về
                </a> 
            ';
  
            // Content thêm chuyến đi
            echo
            '
                <p class="form-add-chuyendi">
                    <form method="POST" id="formAddChuyenDi" onsubmit="return false;">
                        <div class="form-group">
                            <label>Địa Điểm Đón</label>
                            <input type="text" class="form-control title" id="diemdon_add_dschuyendi" placeholder="Nhập địa điếm đón">
                        </div>
                        <div class="form-group">
                            <label>Địa Điểm Đến</label>
                            <input type="text" class="form-control title" id="diemden_add_dschuyendi" placeholder="Nhập địa điếm đến" >
                        </div>
                        <div class="form-group">
                            <label>Thời Gian Đón</label>
                            <input type="text" class="form-control title" id="thoigiandon_add_dschuyendi" placeholder="Nhập thời gian đón" >
                        </div>
                        <div class="form-group">
                            <label>Giá</label>
                            <input type="text" class="form-control title" id="gia_add_dschuyendi" placeholder="Nhập đơn giá" >
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Tạo</button>
                        </div>
                        <div class="alert alert-danger hidden"></div>
                    </form>
                </p>  
            ';
        } 
        // Trang chỉnh sửa chuyến đi
        else if ($ac == 'edit')
        {
            $sql_check_id_cate = "SELECT id_chuyendi FROM danhsachchuyendi WHERE id_chuyendi = '$id'";
            // Nếu tồn tại tham số id trong table
            if ($db->num_rows($sql_check_id_cate)) 
            {
                $data_chuyendi = $db->fetch_assoc($sql_check_id_cate, 1);
 
                if ($data_user['position'] == '1')
                {
                    // Dãy nút của chỉnh sửa chuyến đi
                    echo
                    '
                        <a href="' . $_DOMAIN . 'danhsachchuyendi" class="btn btn-default">
                            <span class="glyphicon glyphicon-arrow-left"></span> Trở về
                        </a>
                        <a class="btn btn-danger" id="del_post" data-id="' . $id . '">
                            <span class="glyphicon glyphicon-trash"></span> Xoá
                        </a> 
                    ';  
      
                    // Content chỉnh sửa chuyến đi
                    $sql_get_data_post = "SELECT * FROM danhsachchuyendi WHERE id_chuyendi = '$id'";
                    $data_post = $db->fetch_assoc($sql_get_data_post, 1);
                    echo
                    '
                        <p class="form-edit-post">
                            <form method="POST" id="formEditChuyenDi" data-id="' . $id . '" onsubmit="return false;">
                                <div class="form-group">
                                    <label>Trạng thái chuyến đi</label>
                    ';
                    
                    // // Trạng thái chuyến đi
                    // Nếu đã cho chạy
                    if ($data_post['status'] == '1') {
                        echo '
                            <div class="radio">
                                <label>
                                    <input type="radio" name="stt_edit_chuyendi" value="1" checked> Active
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="stt_edit_chuyendi" value="0"> Ẩn
                                </label>
                            </div>
                        ';
                    // Nếu đang ẩn
                    } else if ($data_post['status'] == '0') {
                        echo '
                            <div class="radio">
                                <label>
                                    <input type="radio" name="stt_edit_chuyendi" value="1"> Active
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="stt_edit_chuyendi" value="0" checked> Ẩn
                                </label>
                            </div>
                        ';
                    }
                    
                    echo '
                                </div>
                                <div class="form-group">
                                    <label>Địa Điểm Đón</label>
                                    <input type="text" class="form-control title" value="' . $data_post['diadiemdon'] . '" id="diadiemdon_edit_chuyendi">
                                </div>
                                <div class="form-group">
                                    <label>Địa Điểm Đến</label>
                                    <input type="text" class="form-control title" value="' . $data_post['diadiemden'] . '" id="diadiemden_edit_chuyendi">
                                </div>
                                <div class="form-group">
                                    <label>Thời Gian Đón</label>
                                    <input type="text" class="form-control" value="' . $data_post['thoigiandon'] . '" id="thoigiandon_edit_chuyendi">
                                </div>
                                <div class="form-group">
                                    <label>Giá</label>
                                    <input type="text" class="form-control" value="' . $data_post['gia'] . '" id="gia_edit_chuyendi">
                                </div>
                                
                    ';
                    
                   
                    
                    echo '
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Lưu thay đổi</button>
                                </div>
                                <div class="alert alert-danger hidden"></div>
                            </form>
                        </p>
                    '; // End chỉnh sửa chuyến đi
                }
                else
                {
                    echo '<div class="alert alert-danger">ID chuyến đi không thuộc quyền sở hữu của bạn.</div>';
                }
            }
            else
            {
                // Hiển thị thông báo lỗi
                echo
                '
                    <div class="alert alert-danger">ID chuyến đi đã bị xoá hoặc không tồn tại.</div>
                ';
            }
        }
   }
    // Ngược lại không có tham số ac
    // Trang danh sách chuyến đi
    else
    {
        // Dãy nút của danh sách chuyến đi
        echo
        '
            <a href="' . $_DOMAIN . 'danhsachchuyendi/add" class="btn btn-default">
                <span class="glyphicon glyphicon-plus"></span> Thêm
            </a> 
            <a href="' . $_DOMAIN . 'danhsachchuyendi" class="btn btn-default">
                <span class="glyphicon glyphicon-repeat"></span> Reload
            </a> 
            <a class="btn btn-danger" id="del_post_list">
                <span class="glyphicon glyphicon-trash"></span> Xoá
            </a> 
        ';
  
        // Content danh sách chuyến đi
        // Nếu là admin thì lấy toàn bộ chuyến đi
        if ($data_user['position'] == '1') {
            $sql_get_list_chuyendi = "SELECT * FROM danhsachchuyendi ORDER BY id_chuyendi DESC";
        
        }
        // Nếu có chuyến đi
        if ($db->num_rows($sql_get_list_chuyendi))
        {
            // Lấy số trang
            if (isset($_GET['page'])) {
                $current_page = trim(htmlspecialchars(addslashes($_GET['page']))); 
            } else {
                $current_page = '';
            }
        
            $limit = 10; // Giới hạn số chuyến đi trong 1 trang
            $total_page = ceil($db->num_rows($sql_get_list_chuyendi) / $limit); // Tổng trang
            $start = ($current_page - 1) * $limit; // Vị trí bắt đầu lấy trang
        
            // Nếu số trang hiện tại > tổng trang
            if ($current_page > $total_page) {
                new Redirect($_DOMAIN . 'danhsachchuyendi&page=' . $total_page); // Tới số trang lớn nhất
            // Nếu số trang hiện tại < 1
            } else if ($current_page < 1){
                new Redirect($_DOMAIN . 'danhsachchuyendi&page=1'); // Tới trang đầu tiên
            }   
        
            // Form tìm kiếm
            echo
            '
                <p>
                    <form method="POST" id="formSearchChuyenDi" onsubmit="return false;">
                        <div class="input-group">         
                            <input type="text" class="form-control" id="kw_search_chuyendi" placeholder="Nhập ID, Nơi Đi, Nơi Đến ...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                    </form>
                </p>
            ';
        
            echo
            '
                <div class="table-responsive" id="list_chuyendi">
                    <table class="table table-striped list">
                        <tr>
                            <td><input type="checkbox"></td>
                            <td><strong>ID</strong></td>
                            <td><strong>Địa Điểm Đón</strong></td>
                            <td><strong>Địa Điểm Đến</strong></td>
                            <td><strong>Trạng Thái</strong></td>
                            <td><strong>Thời Gian Đón</strong></td>
                            <td><strong>Giá</strong></td>
            ';
        
            // Nếu tài khoản là admin
            // if ($data_user['position'] == '1') {
            //     echo '<td><strong>Lượt Chạy</strong></td>';
            // }
        
            echo '
                        <td><strong>Tools</strong></td>
                        </tr>
            ';
        
            
            // Nếu là admin thì lấy toàn bộ chuyến đi
            if ($data_user['position'] == '1') {
                $sql_get_list_post_limit = "SELECT * FROM danhsachchuyendi ORDER BY id_chuyendi DESC LIMIT $start, $limit";
            // Nếu là tác giả thì chỉ lấy những bài thuộc sở hữu
            } 
            // In danh sách chuyến đi
            foreach ($db->fetch_assoc($sql_get_list_post_limit, 0) as $key => $data_chuyendi) 
            {
                // Trạng thái chuyến đi
                if ($data_chuyendi['status'] == 0) {
                    $stt_chuyendi = '<label class="label label-warning">Ẩn</label>';
                } else if ($data_chuyendi['status'] == 1) {
                    $stt_chuyendi = '<label class="label label-success">Active</label>';
                }
        
                // Chuyên mục chuyến đi
                // $cate_post = '';
                // $sql_check_id_cate_1 = "SELECT label, id_cate FROM categories WHERE id_cate = '$data_chuyendi[cate_1_id]' AND type = '1'";
                // if ($db->num_rows($sql_check_id_cate_1)) {
                //     $data_cate_1 = $db->fetch_assoc($sql_check_id_cate_1, 1);
                //     $cate_post .= $data_cate_1['label'];
                // } else {
                //     $cate_post .= '<span class="text-danger">Lỗi</span>';
                // }
        
                // $sql_check_id_cate_2 = "SELECT label, id_cate FROM categories WHERE id_cate = '$data_chuyendi[cate_2_id]' AND type = '2'";
                // if ($db->num_rows($sql_check_id_cate_2)) {
                //     $data_cate_2 = $db->fetch_assoc($sql_check_id_cate_2, 1);
                //     $cate_post .= ', ' . $data_cate_2['label'];
                // } else {
                //     $cate_post .= ', <span class="text-danger">Lỗi</span>';
                // }
        
                // $sql_check_id_cate_3 = "SELECT label, id_cate FROM categories WHERE id_cate = '$data_chuyendi[cate_3_id]' AND type = '3'";
                // if ($db->num_rows($sql_check_id_cate_3)) {
                //     $data_cate_3 = $db->fetch_assoc($sql_check_id_cate_3, 1);
                //     $cate_post .= ', ' . $data_cate_3['label'];
                // } else {
                //     $cate_post .= ', <span class="text-danger">Lỗi</span>';
                // }
        
                // Tác giả chuyến đi
                // $sql_get_author = "SELECT display_name FROM accounts WHERE id_acc = '$data_chuyendi[author_id]'";
                // if ($db->num_rows($sql_get_author)) {
                //     $data_author = $db->fetch_assoc($sql_get_author, 1);
                //     $author_post = $data_author['display_name'];
                // } else {
                //     $author_post = '<span class="text-danger">Lỗi</span>';
                // }
        
                echo
                '
                    <tr>
                        <td><input type="checkbox" name="id_chuyendi[]" value="' . $data_chuyendi['id_chuyendi'] .'"></td>
                        <td>' . $data_chuyendi['id_chuyendi'] . '</td>
                        <td>' . $data_chuyendi['diadiemdon'] . '</td>
                        <td>' . $data_chuyendi['diadiemden'] . '</td>
                        <td>' . $stt_chuyendi . '</td>
                        <td>' . $data_chuyendi['thoigiandon'] . '</td>
                        <td>' . $data_chuyendi['gia'] . ' VNĐ</td>
                ';
        
                // // Tác giả chuyến đi
                // if ($data_user['position'] == '1') {
                //     echo '<td>' . $author_post . '</td>';
                // }
        
                echo '
                        <td>
                            <a href="' . $_DOMAIN . 'danhsachchuyendi/edit/' . $data_chuyendi['id_chuyendi'] .'" class="btn btn-primary btn-sm">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a class="btn btn-danger btn-sm del-post-list" data-id="' . $data_chuyendi['id_chuyendi'] . '">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                ';
            }
        
            echo
            '
                    </table>
            ';
        
            // Nút phân trang
            echo '<div class="btn-group" id="paging_chuyendi">';
            // Nếu trang hiện tại > 1 và tổng trang > 1 thì hiển thị nút prev
            if ($current_page > 1 && $total_page > 1){
                echo '<a class="btn btn-default" href="' . $_DOMAIN . 'danhsachchuyendi&page=' . ($current_page - 1) . '"><span class="glyphicon glyphicon-chevron-left"></span> Prev</a>';
            }
            
            // In số nút trang
            for ($i = 1; $i <= $total_page; $i++){
                // Nếu trùng với trang hiện tại thì active
                if ($i == $current_page){
                    echo '<a class="btn btn-default active">' . $i . '</a>';
                // Ngược lại
                } else {
                    echo '<a class="btn btn-default" href="' . $_DOMAIN . 'danhsachchuyendi&page=' . $i . '">' . $i . '</a>';
                }
            }
            
            // Nếu trang hiện tại < tổng số trang > 1 thì hiển thị nút next
            if ($current_page < $total_page && $total_page > 1){
                echo '<a class="btn btn-default" href="' . $_DOMAIN . 'danhsachchuyendi&page=' . ($current_page + 1) . '">Next <span class="glyphicon glyphicon-chevron-right"></span></a>';
            }
            echo '<br><br><br></div>';
        
            echo '
                </div>
            ';
        }
        // Nếu không có chuyến đi
        else
        {
            echo '<br><br><div class="alert alert-info">Chưa có chuyến đi nào.</div>';
        }
    }
}
// Ngược lại chưa đăng nhập
else
{
    new Redirect($_DOMAIN); // Trở về trang index
}
  
?>